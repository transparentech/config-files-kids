# Kids Standard Config Files

This repository contains configuration files for the accounts on the kids
computer. Many of them are "dot" files that can be linked into the HOME folder
using the `setup.sh` script. Others (such as `iterm-profile.json`) can be
imported into the equivalent application.

## setup.sh

Use this script to link the "dot" files into your HOME directory.

NOTE: this script should be run from the kids account!
