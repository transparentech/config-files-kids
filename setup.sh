#!/bin/bash
#
# Link the config files from this repository into the user's HOME directory.
#

# Files from this repository to link
CFG_FILES=(.gitconfig .gitignore_global .spacemacs.d .zshrc)
# The HOME directory.
HOME_PATH=$HOME

# The directory of this script - same as the files to link.
# Note: if greadlink is unknown, install with: brew install coreutils
if ! [ -x "$(command -v greadlink)" ]; then
    echo "ERROR: greadlink not found. Please run 'brew install coreutils'."
    exit 1
fi
LOCAL_PATH="$(dirname "$(greadlink -f "$0")")"

# Loop over the files and link them into the Home directory.
for f in "${CFG_FILES[@]}"
do
    # First remove the existing file/link.
    cmd="rm -rf $HOME_PATH/$f"
    echo $cmd
    $cmd
    # Then link in the current repository file.
    cmd="ln -sF $LOCAL_PATH/$f $HOME_PATH/$f"
    echo $cmd
    $cmd
done
