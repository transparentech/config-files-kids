(defun my-comment-or-uncomment-region-or-line ()
  "Comments/uncomments the region or the current line like in Intellij."
  (interactive)
  (if (region-active-p)
      (comment-or-uncomment-region (region-beginning) (region-end))
    (comment-line 1)))

(defun my-forward-kill-whitespace-or-word ()
  "Kill to the end of the word or end of the whitespace similar to (but not exactly like) Intellij."
  (interactive)
  ;; If there's just whitespace till the end of the line, delete it all including the EOL.
  (if (looking-at "[[:space:]]*$")
      (kill-line 1)
    ;; If there is just whitespace next, delete upto next next non-blank space (i.e. don't delete EOL).
    (if (looking-at "[[:space:]]")
        (let ((pos (point)))
          (re-search-forward "[^ \t\v\f]" nil t)
          (backward-char)
          (kill-region pos (point)))
      ;; Otherwise there is non-whitespace next, so just delete word.
      (kill-word 1))))
