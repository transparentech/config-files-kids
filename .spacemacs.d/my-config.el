;; Set the size and position of the Emacs window. height=58 is max on MBP built-in screen for my resolution.
(setq initial-frame-alist '((top . 0) (left . 0) (width . 140) (height . 58)))

;; Use the Command key as the meta so M-m or M-x is ⌘-m or ⌘-x
(setq mac-command-modifier 'meta)

;; Treat the selection like in other apps - Delete key will delete it; yanking will replace it.
(delete-selection-mode 1)

;; Turn off the auto-revert stuff in the electric-buffer-list (prevents cursor jumping back to top).
(add-to-list 'global-auto-revert-ignore-modes 'electric-buffer-menu-mode)

;; Use the electric-buffer-list for viewing the buffers.
(global-set-key "\C-x\C-b"   'electric-buffer-list)
;; Intellij/Eclipse like comment-uncomment on ctrl-7.
(global-set-key (kbd "C-7")  'my-comment-or-uncomment-region-or-line)
;; Intellij similar forward-kill of whitespace or word.
(global-set-key (kbd "M-d")  'my-forward-kill-whitespace-or-word)
(global-set-key "\C-cl"      'goto-line)
(global-set-key (kbd "<f8>") 'treemacs)

;; persistent-scratch - needs to be included in dotspacemacs-additional-packages
(persistent-scratch-setup-default)
