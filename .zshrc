# ZSH info: https://wiki.archlinux.org/index.php/Zsh

# Note that the -u for compinit is required in multi-user systems to avoid warnings.
autoload -U compinit && compinit -u
autoload -U colors && colors

zstyle ':completion:*' squeeze-slashes true #I don’t want it to think that // means anything special, though:
local WORDCHARS=${WORDCHARS//\//} # Use forward slash as a boundary character

# Use emacs key bindings
bindkey -e

PROMPT="%{$fg[green]%}%n%{$reset_color%}: %~> "

setopt allexport #- assumes all variable assignments are exported to the environment. Greatly reduced the redundancy of having "export" on many lines in .zshrc.
setopt autocd #- assume the 'cd' command if just a path is specified on the command line.
setopt pushdignoredups #- ignores directory stack duplicates.
setopt histignoredups #- keeps duplicates out of the history.
#setopt interactivecomment #- allows comments in the interactive shell. Useful for pasting.
setopt promptsubst #- parameter, command, and arithmetic substitution are performed in prompts.
setopt extendedglob # enable extended globbing.
setopt nocheckjobs          #don't warn about bg processes on exit
setopt nohup                #don't kill bg processes on exit

# Need for iTerm2 on Mac as there it gets set to just UTF-8 and then sphinx doesn't work.
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# External editors
export ALTERNATE_EDITOR=emacs
export EDITOR=emacsclient
export VISUAL=emacsclient

export PATH=/usr/local/bin:/usr/local/sbin:/opt/local/bin:/usr/bin:/bin:/usr/sbin:/sbin
export PATH=${PATH}:/Library/TeX/texbin

alias ls='ls -aF' # --color=auto --show-control-chars'
alias ll='ls -l'
alias rm='rm -i'
alias pss='ps aux'

alias gba="git branch -avv"
alias gbs="git status"
alias wgeto="wget -qO-"
